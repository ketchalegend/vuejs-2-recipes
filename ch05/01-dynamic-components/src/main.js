import Vue from 'vue/dist/vue.js';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.css';

Vue.config.productionTip = false;

new Vue({
  data() {
    return {
      myProperty: 'Hello from the root!'
    };
  },
  render: h => h(App)
}).$mount('#app');
