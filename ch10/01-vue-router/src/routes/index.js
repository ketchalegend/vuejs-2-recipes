export default [
  {
    path: '/home',
    name: 'home',
    component: () => import('@/pages/Home'),
    meta: {
      auth: false,
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/pages/Login'),

    meta: {
      guest: true,
    }
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import('@/pages/Contact'),
    meta: {
      auth: false,
    }
  },
  {
    path: '/super-secret',
    name: 'super-secret',
    component: () => import('@/pages/SuperSecret'),
    meta: {
      auth: false,
    }
  },
  {
    path: '/account',
    name: 'account',
    component: () => import('@/pages/Account'),
    redirect: '/account/information',
    meta: {
      auth: true,
    },
    children: [
      {
        path: 'information',
        name: 'account.information',
        component: () => import('@/pages/Account/Information'),
        meta: {
          auth: true,
        }
      },
      {
        path: 'following',
        name: 'account.following',
        component: () => import('@/pages/Account/Following'),
        meta: {
          auth: true,
        }
      },
      {
        path: 'posts',
        name: 'account.posts',
        component: () => import('@/pages/Account/Posts'),
        meta: {
          auth: true,
        }
      }
    ]
  },
  {
    path: '/posts',
    name: 'posts.index',
    component: () => import('@/pages/posts/Index'),
    meta: {
      auth: false,
    }
  },
  {
    path: '/posts/:id',
    name: 'posts.show',
    component: () => import('@/pages/posts/Show'),
    // Set the props-property to true to send parameters as properties.
    props: true,
    meta: {
      auth: false,
    }
  },
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/*',
    redirect: '/home',
  },
];
