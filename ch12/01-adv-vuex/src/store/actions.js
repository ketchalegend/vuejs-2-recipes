import * as types from './mutation-types';

export default {
  fetchTodos({ commit }) {
    setTimeout(() => {
      commit(types.FETCH_TODOS, [
        {
          id: 1,
          title: 'Hello World!',
          completed: false,
        },
        {
          id: 2,
          title: 'Hello Back!',
          completed: false,
        },
        {
          id: 2,
          title: 'John Doe',
          completed: false,
        }
      ]);
    }, 1000);
  },
  removeTodos({ commit }) {
    setTimeout(() => {
      commit(types.REMOVE_TODOS);
    }, 1000);
  }
}
