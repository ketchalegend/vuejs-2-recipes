import {ADD_TODO, REMOVE_TODO, TOGGLE_TODO, FETCH_TODOS, REMOVE_TODOS} from './mutation-types';

export default {
  [ADD_TODO](state, todo) {
    // Create a copy of the todos array and grab the last item.
    const lastTodo = state.todos.slice(-1).pop();

    state.todos.push({
      title: todo,
      completed: false,
      id: lastTodo ? lastTodo.id + 1 : 1,
    });
  },
  [REMOVE_TODO](state, todo) {
    state.todos = state.todos.filter(item => item !== todo);
  },
  [TOGGLE_TODO](state, todo) {
    const foundTodo = state.todos.find(item => item === todo);

    foundTodo.completed = !foundTodo.completed;
  },
  [FETCH_TODOS](state, todos) {
    state.todos = todos;
  },
  [REMOVE_TODOS](state) {
    state.todos = [];
  },
};
