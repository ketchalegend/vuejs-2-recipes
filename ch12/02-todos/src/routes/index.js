export default [
  {
    path: '/posts',
    name: 'posts',
    component: () => import('@/pages/Posts'),
  },

  {
    path: '/todos',
    name: 'todos',
    component: () => import('@/pages/Todos'),
  },

  {
    path: '/',
    redirect: '/posts',
  },

  {
    path: '/*',
    redirect: '/posts',
  },
];
