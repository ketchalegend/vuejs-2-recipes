import Vue from 'vue';
import App from './App.vue';
import './plugins/vuex';
import './plugins/axios';
import {router} from './plugins/vue-router';
import './plugins/vuex-router-sync';
import './plugins/bootstrap';
import 'font-awesome/css/font-awesome.css';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
