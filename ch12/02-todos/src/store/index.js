import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger'
import createPersistedState from 'vuex-persistedstate'
import post from './modules/post';
import todo from './modules/todo';
import todoForm from './modules/todo-form';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';
const persistedStateConfig = {
  key: 'store',
};

export default new Vuex.Store({
  modules: {
    post,
    todo,
    todoForm,
  },
  strict: debug,
  plugins: debug ?
    [
      createLogger(),
      createPersistedState(persistedStateConfig),
    ] :
    [
      createPersistedState(persistedStateConfig)
    ]
});
