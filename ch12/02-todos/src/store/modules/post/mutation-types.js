export const FETCH = 'FETCH';
export const CLEAR = 'CLEAR';

export default {
  FETCH,
  CLEAR,
};
