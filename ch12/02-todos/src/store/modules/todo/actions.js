import Vue from 'vue';
import * as types from './mutation-types';

export default {
  fetch({ commit }) {
    Vue.$http
      .get('todos')
      .then(({ data }) => {
        commit(types.FETCH, data);
      })
      .catch((error) => {
        /* eslint-disable no-console */
        console.log(error);
      });
  },
  clear({ commit }) {
    commit(types.CLEAR);
  },
}
