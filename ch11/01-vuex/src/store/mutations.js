export default {
  increment(state) {
    state.count++;
  },
  decrement(state) {
    if(state.count === 0) {
      return;
    }
    state.count--;
  },
  addAlert(state, alert) {
    state.alertMessages.push(alert);
  },
  removeAlert(state, alert) {
    state.alertMessages = state.alertMessages.filter(item => item !== alert);
  },
};
