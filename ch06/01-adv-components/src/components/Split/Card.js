export default {
  /**
   * The name of this Vue component
   */
  name: 'card-component',

  /**
   * The properties this Vue component can use
   */
  props: {
    header: {
      type: String,
      required: false,
    },
    body: {
      type: String,
      required: false,
    },
    footer: {
      type: String,
      required: false,
    },
  },
};
